#include <windows.h>
#include <tchar.h>

// /MERGE:.rdata=.text も含めたら avast にウィルス扱いされた......おのれ avast!!
#pragma comment(linker, "/nodefaultlib /MERGE:.data=.text /SECTION:.text,EWR /opt:nowin98")
#define ARRAY_SIZE(x)	(sizeof(x) / sizeof(x[0]))

const TCHAR USAGE_MESSAGE[] =
_T("Usage: snotify [/ewidbh]\n")
_T("Play a notification sound that specified in Windows system.\n")
_T("\n")
_T("  /e Play error sound\n")
_T("  /w Play warning sound\n")
_T("  /i Play information sound\n")
_T("  /d Play default sound (default)\n")
_T("  /b Play beep sound\n")
_T("  /h Show usage.\n")
_T("\n")
_T("v1.0.0 2014-08-28 02:27 (JST)\n")
_T("http://ir9.jp/\n")
_T("author: irokyu")
;

void my_putts(const TCHAR* msg, DWORD length)
{
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	if(hOut == INVALID_HANDLE_VALUE)
		return;	
	WriteConsole(hOut, msg, length, NULL, NULL);
}


BOOL ParseArg(const TCHAR* const argv, DWORD* notifyType)
{
	const TCHAR* arg = argv;
	
	while(*arg)
	{
		if(*arg == _T('-') || *arg == _T('/'))
		{
			++arg;
			switch(*arg)
			{
			case _T('E'):
			case _T('e'):	*notifyType = MB_ICONERROR;			break;	// error
			case _T('W'):
			case _T('w'):	*notifyType = MB_ICONWARNING;		break;	// warning
			case _T('I'):
			case _T('i'):	*notifyType = MB_ICONINFORMATION;	break;	// information
			case _T('Q'):
			case _T('q'):	*notifyType = MB_ICONQUESTION;		break;	// question
			case _T('D'):
			case _T('d'):	*notifyType = MB_OK;				break;	// ok
			case _T('B'):
			case _T('b'):	*notifyType = 0xFFFFFFFF;			break;	// beep
			case _T('H'):
			case _T('h'):	my_putts(USAGE_MESSAGE, ARRAY_SIZE(USAGE_MESSAGE) - 1); 	return FALSE;	// help
			}
		}
		++arg;
	}
	
	return TRUE;
}

int mainCRTStartup()
{
	DWORD notifyType = MB_OK;
	if(!ParseArg(GetCommandLine(), &notifyType))
		return 0;
	MessageBeep(notifyType);
	return 0;
}





